<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Halaman Form</title>
</head>
<body>
    <h1>Buat Account Baru!</h1>
    <h3>Sign Up Form</h3>
    <form action="/welcome" method="post">
        @csrf
        <label>First Name:</label><br><br>
        <input type="text" name="first_name"><br><br>

        <label>Last Name:</label><br><br>
        <input type="text" name="last_name"><br><br>

        <label>Gender:</label><br><br>
        <input type="radio" name="gender" value="1">Male<br>
        <input type="radio" name="gender" value="2">Female<br>
        <input type="radio" name="gender" value="3">Other<br><br>

        <label>Nationality:</label><br><br>
        <select name="nationality">
            <option value="1">Indonesian</option>
            <option value="2">Singapore</option>
            <option value="3">Malaysian</option>
            <option value="4">Australian</option>
        </select> <br><br>
        
        <label>Language Spoken:</label><br><br>
        <input type="checkbox" name="bahasa" value="1">Bahasa Indonesia<br>
        <input type="checkbox" name="bahasa" value="2">English<br>
        <input type="checkbox" name="bahasa" value="3">Other<br> <br>

        <label>Bio:</label><br><br>
        <textarea name="bio" cols="30" rows="10"></textarea><br>

        <input type="submit" value="sign up">

    </form>
    
</body>
</html>