<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function register()
    {
        return view('register');    
    }
    
    public function kirim(Request $request)
    {
        $namaDepan = $request['first_name'];
        $namaBelakang = $request['last_name'];

        return view('welcome',['namaDepan' => $namaDepan, 'namaBelakang'=> $namaBelakang]);

    }

}
