<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\TableController;
use App\Http\Controllers\CastController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [HomeController::class, 'home']);
    
Route::get('/register', [AuthController::class, 'register']);

Route::post('/welcome', [AuthController::class, 'kirim']);

Route::get('/table', [TableController::class, 'table']);

Route::get('/data-table', [TableController::class, 'Datatable']);

//CRUD

//CREATE
//form tambah cast
Route::get('/cast/create', [CastController::class, 'create']);
//fungsi tambah data cast
Route::post('/cast', [CastController::class, 'store']);

//READ
//Tampil Semua 
Route::get('/cast', [CastController::class, 'index']);

//detail cast berdasarkan id
Route::get('/cast/{cast_id}', [CastController::class, 'show']);


//Update
Route::get('/cast/{cast_id}/edit', [CastController::class, 'edit']);

//update data berdasarkan id 
Route::put('/cast/{cast_id}', [CastController::class, 'update']);

//Delete
//delete berdasarkan id
Route::delete('/cast/{cast_id}', [CastController::class, 'delete']);

    


