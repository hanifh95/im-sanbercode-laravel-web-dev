<?php
require_once('animal.php');
require_once('frog.php');
require_once('Ape.php');

$sheep = new Animal ("shaun");

echo "Name : " . $sheep->nama . "<br>";
echo "legs : " . $sheep->legs . "<br>";
echo "cold_blooded  : " . $sheep->cold_blooded . "<br><br>";


$kodok = new Frog("buduk");

echo "Name : " . $kodok->nama . "<br>";
echo "legs : " . $kodok->legs . "<br>";
echo "cold_blooded  : " . $kodok->cold_blooded . "<br>";
echo "Jump  : " . $kodok->Jump() . "<br><br>";



$sungokong = new Ape("kera sakti");

echo "Name : " . $sungokong->nama . "<br>";
echo "legs : " . $sungokong->legs . "<br>";
echo "cold_blooded  : " . $sungokong->cold_blooded . "<br>";
echo "Yell  : " . $sungokong->yell() . "<br><br>";



?>

